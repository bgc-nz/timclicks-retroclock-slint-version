use chrono::{self,Timelike};
use skia_safe::gpu::gl::{Interface, FramebufferInfo};
use skia_safe::gpu::DirectContext;
use std::{rc::Rc};
use slint::*;
use skia_safe::*;

slint::include_modules!();

fn update_clock(clock: &mut Clock) {
    let now = chrono::Local::now();
    let hour = now.hour() as f32;
    let minute = now.minute() as f32;
    let second = now.second() as f32;
    let second_angle = (360.0 / 60.0) * second;
    let minute_angle = (360.0 / 60.0) * minute;
    let hour_angle: f32 = (360.0 / 12.0) * (hour % 12.);
    clock.hour = hour;
    clock.minute = minute;
    clock.second = second;
}
pub fn main() {
    let main_window = Rc::new(AppWindow::new().unwrap());
    // let main_window_rc = Arc::new(AppWindow::new().unwrap());
    let main_window_weak = main_window.as_weak();
    let main_window_clone = main_window.clone_strong();
    let timer = Timer::default();
    let mut opengl_context: Option<glow::Context> = None;
    let mut current_clock: Clock = Clock { hour:0.0, minute: 0.0, second: 0.0 };
    //let mut grgl_interface: RCHandle<> = RCHandle::from(std::ptr::null());
    let mut gr_context: Option<DirectContext> = None;
    let mut skia_canvas: Option<&skia_safe::Canvas> = None;
    let mut skia_surface: Option<skia_safe::Surface> = None;
    //let mut skia_surface: Rc<skia_safe::Surface>;
    timer.start(TimerMode::Repeated, 
        std::time::Duration::from_secs(1), 
           move || {
            update_clock(&mut current_clock);
            main_window_weak.unwrap().set_current_clock(current_clock.clone());
            main_window_weak.unwrap().window().request_redraw();
    });
    main_window_clone.window().set_rendering_notifier(  move|state, graphics_api| {
        match state {
            slint::RenderingState::RenderingSetup => {
                let grgl_interface = match graphics_api {
                    slint::GraphicsAPI::NativeOpenGL { get_proc_address} => {
                        println!("Slint Graphics API is using native OpenGL");
                        skia_safe::gpu::gl::Interface::new_load_with_cstr(|s| get_proc_address(s)).unwrap()
                        // glow::Context::from_loader_function_cstr(|s| get_proc_address(s))
                    },
                    _ => unreachable!(),
                };
                // Refer to https://github.com/rust-skia/rust-skia/blob/master/skia-safe/examples/gl-window/main.rs#L173
                gr_context = Some(skia_safe::gpu::DirectContext::new_gl(Some(grgl_interface), None)
                    .expect("Failed to create GPU direct context for Skia"));
                let window_size = main_window.window().size();
                println!("{:?}",window_size);
                skia_surface = Some(create_skia_surface(
                    window_size.width as i32,
                     window_size.height as i32, gr_context.as_mut()
                     .expect("Failed to create Skia surface")));
                println!("{:?}", skia_surface.to_owned().unwrap());
            },
            slint::RenderingState::BeforeRendering => {
                let mut blargh = skia_surface.to_owned().expect("Skia surface not available");
                let mut paint = skia_safe::Paint::default();
                paint.set_argb(255, 12, 85, 178);
                paint.set_style(PaintStyle::Fill);
                blargh.canvas().draw_rect(skia_safe::Rect::new(0.0,0.0, 300.0, 300.0), &paint);
                blargh.canvas().draw_circle((200,200), 350.0, &paint);
                main_window.window().request_redraw();
            }
            slint::RenderingState::AfterRendering => {
                
            }
            slint::RenderingState::RenderingTeardown => {
                
            }
            _ => {}
        }
        //process_rendering(state, &main_window_clone);
    }).expect("Couldn't add rendering notifier");
    main_window_clone.run().unwrap();
    // main_window.run().unwrap();
}

fn create_skia_surface(window_width: i32, 
    window_height: i32,
    gr_context: &mut skia_safe::gpu::DirectContext) -> Surface
{
    let window_size = (
        window_width,
        window_height,
    );
    // let backend_render_target = skia_safe::gpu::backend_render_targets::make_gl(
    //     window_size, None, stencil_size, fb_info);
    let skia_imageinfo = skia_safe::ImageInfo::new_n32_premul(window_size, None);
    gpu::surfaces::render_target(
        gr_context, skia_safe::gpu::Budgeted::No,
         &skia_imageinfo, 
        0, gpu::SurfaceOrigin::BottomLeft, 
        None, false, 
        false).expect("Could not create Skia surface")
    // gpu::surfaces::wrap_backend_render_target(
    //     gr_context,
    //     &backend_render_target,
    //     gpu::SurfaceOrigin::BottomLeft,
    //     ColorType::RGBA8888,
    //     None,
    //     None,
    // ).expect("Could not create Skia surface")
}

fn redraw_underlay_clock_image(context: glow::Context) {
    unsafe {
        let gl = context;
    }
}

// fn process_rendering(state: RenderingState, main_window: &AppWindow) {
//     match state {
//         RenderingState::BeforeRendering => {
//             main_window.set_canvas_source(
//                 render_image(main_window.window().size().width,
//                              main_window.window().size().height)
//             );
//         }
//         _ => {}
//     }
// }


//  See https://docs.rs/slint/latest/slint/struct.Image.html.
// This is an only slightly modified version, so that the circle is always
// in the windows center, with arbitrary window sizes.

// fn render_image(window_width: u32, window_height: u32) -> slint::Image {
//     let mut buffer = SharedPixelBuffer::<Rgba8Pixel>::new(window_width, window_height);
//     let mut paint = Paint::default();
//     let mut pixmap = Pixmap::new();
//     slint::Image::from_rgba8_premultiplied(pixel_buffer);
// }