# Retroclock (Slint version of timclicks Retroclock)
Inspired by retro-futuristic-clock by timclicks: https://github.com/timClicks/tutorials/tree/main/retro-futuristic-clock, I'm attempting to use Slint, which is a user interface crate started by former Qt project leads, and borrows ideas from Qt Quick.

## Backstory
I've wanted to improve my Rust, and Tim's example got me keen, so I decided to try building a version with Slint. It quickly became more difficult, because I wanted to perform drawing utilized by Skia, which Slint uses internally.

As of writing, Slint currently does not expose a Skia canvas directly, so with my rudimentary Rust knowledge, I tried to derive one from a Surface, derived from a DirectContext.

After struggling with borrowing and memory management patterns in Rust, fairly terribly, I've got a working program, however, the drawing isn't showing up yet.

## Highlights
It took me a while to grasp the API style used by skia_safe.

Even though understanding memory ownership confused me, the Rust compiler provided clear error explanations.